from django.conf.urls import url, include
from django.contrib import admin



from . import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^(?P<tcomplex_id>[0-9]+)/$', views.detail, name='detail'),
]
