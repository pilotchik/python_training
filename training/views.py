from django.shortcuts import  get_object_or_404,render
from django.http import HttpResponse
from django.template import RequestContext, loader

from .models import Complex

def index(request):
    latest_question_list = Complex.objects.order_by('-ts')[:5]
    template = loader.get_template('training/index.html')
    context = RequestContext(request, {
        'latest_question_list': latest_question_list,
    })
    return HttpResponse(template.render(context))

def detail(request,tcomplex_id):
    tcomplex = get_object_or_404(Complex, pk=tcomplex_id)
    return render(request, 'training/detail.html', {'tcomplex': tcomplex})