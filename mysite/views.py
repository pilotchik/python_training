from django.shortcuts import  get_object_or_404,render
from django.http import HttpResponse
from django.template import RequestContext, loader


def complex(request):
    tcomplex = [
        {'name': 'orlan', 'description': 3},
        {'name': 'forpost', 'description': 5}
    ]

    a = 2 * 5

    a = 5
    b = a

    if a > 5:
        a = a - 5

    template = loader.get_template('training/detail.html')
    context = RequestContext(request, {
        'tcomplex': tcomplex,
        'result' : b
    })
    return HttpResponse(template.render(context))